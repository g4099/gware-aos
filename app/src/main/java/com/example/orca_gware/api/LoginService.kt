package com.example.orca_gware.api

import com.example.orca_gware.model.UserSignInReq
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.*

interface LoginService {
//    @POST("/api/orca_gware_user/signIn")
//    fun login(
//        @Field("email") email:String,
//        @Field("password") password:String
//    ) : Call

    @POST("/api/orca_gware_user/signIn")
    fun postSignIn(
        @Body userSignInReq: UserSignInReq
    ) : Single<JsonObject?>?


    //GET방식 참고자료
//    @GET("/api/{user_idx}")
//    fun getUserInfo(
//        @Query ("user_idx") userIdx:Int
//    ) : Single<JsonObject?>?
}