package com.example.orca_gware.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserSignInReq(
    @Expose
    @SerializedName("user_email")
    var userEmail:String,
    @Expose
    @SerializedName("user_pwd")
    var userPwd:String
)
