package com.example.orca_gware.fragment

import android.app.AlertDialog
import android.content.Context
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.OnBackPressedCallback
import com.example.orca_gware.MainActivity
import com.example.orca_gware.R
import com.example.orca_gware.api.LoginService
import com.example.orca_gware.databinding.FragmentLoginBinding
import com.example.orca_gware.model.UserSignInReq
import com.example.orca_gware.network.ApiService
import com.example.orca_gware.view.LoginActivity
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class LoginFragment : Fragment() {
    companion object{
        const val TAG = "LoginFragment"
    }

    private var _binding : FragmentLoginBinding? = null
    private val binding get() = _binding!!
    var disposable = CompositeDisposable()
    lateinit var loginService: LoginService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentLoginBinding.inflate(inflater,container,false)
        loginService = ApiService.provideApi(LoginService::class.java, requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.txtPasswordFind.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        binding.btnRegister.setOnClickListener {
            val builder = AlertDialog.Builder(activity)
            val customlayout = layoutInflater.inflate(R.layout.custom_dialog, null)

            val btnRegister = customlayout.findViewById<Button>(R.id.btnRegisterClick)
            builder.setView(customlayout)
            val alert = builder.show()
            btnRegister.setOnClickListener {
                val loginActivity = activity as LoginActivity
                loginActivity.setFrag("Terms")
                alert.dismiss()
            }
        }

        binding.btnLogin.setOnClickListener {
            callUserSignInApi()
        }
    }

    fun callUserSignInApi(){
        disposable.clear()
        var email = binding.etEmail.text.toString()
        var pwd = binding.etPassword.text.toString()
        var userSignInReq = UserSignInReq(email, pwd)
        disposable.add(
            loginService.postSignIn(userSignInReq)!!.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object: DisposableSingleObserver<JsonObject?>(){
                    override fun onSuccess(t: JsonObject) {
                        Log.d(TAG, "onSuccess: $t")
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }
                })
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}