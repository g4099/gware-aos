package com.example.orca_gware.fragment

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.orca_gware.R
import com.example.orca_gware.databinding.FragmentBusinessNumBinding
import com.example.orca_gware.view.LoginActivity

class BusinessNumFragment : Fragment() {

    private var _binding : FragmentBusinessNumBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentBusinessNumBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnNext.setOnClickListener {
            val builder = AlertDialog.Builder(activity)
            val customlayout = layoutInflater.inflate(R.layout.custom_dialog, null)
            val btnRegister = customlayout.findViewById<Button>(R.id.btnRegisterClick)
            builder.setView(customlayout)

            val alert = builder.show()
            btnRegister.setOnClickListener {
                alert.dismiss()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }
}