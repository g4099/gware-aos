package com.example.orca_gware.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.orca_gware.R
import com.example.orca_gware.databinding.FragmentPhoneNumCheckBinding
import com.example.orca_gware.view.LoginActivity


class PhoneNumCheckFragment : Fragment() {

    private var _binding : FragmentPhoneNumCheckBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentPhoneNumCheckBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnNext.setOnClickListener {
            val loginActivity = activity as LoginActivity
            loginActivity.setFrag("BusinessNum")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}