package com.example.orca_gware.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.orca_gware.R
import com.example.orca_gware.databinding.FragmentSignUpBinding
import com.example.orca_gware.view.LoginActivity

class SignUpFragment : Fragment() {

    private var _binding : FragmentSignUpBinding? = null
    private val binding get() = _binding!!
    lateinit var pwd1 : String
    lateinit var pwd2 : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSignUpBinding.inflate(inflater,container,false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.btnNext.setOnClickListener {
            pwd1 = binding.etPassword.text.toString()
            pwd2 = binding.etPassword2.text.toString()
            if(pwd1!=null && pwd2!=null){
                if(pwd1==pwd2){
                    val loginActivity = activity as LoginActivity
                    loginActivity.setFrag("PhoneNum")
                }else {
                    Toast.makeText(activity, "비밀번호가 일치하지 않습니다.", Toast.LENGTH_SHORT).show()
                }
            }else {
                Toast.makeText(activity, "비밀번호를 입력해주세요", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}