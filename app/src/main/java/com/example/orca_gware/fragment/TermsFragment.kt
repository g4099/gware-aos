package com.example.orca_gware.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import com.example.orca_gware.R
import com.example.orca_gware.databinding.FragmentTermsBinding
import com.example.orca_gware.view.LoginActivity
import kotlin.math.log

class TermsFragment : Fragment() {

    private var _binding : FragmentTermsBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentTermsBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cbAll.setOnCheckedChangeListener(listener)
        binding.btnAgree.setOnClickListener {
            if(binding.cbAgree1.isChecked==false || binding.cbAgree2.isChecked==false){
                Toast.makeText(activity, "필수 약관에 동의하셔야 합니다.", Toast.LENGTH_SHORT).show()
            }else {
                val loginActivity = activity as LoginActivity
                loginActivity.setFrag("SignUp")
            }
        }

        binding.imgBtnBack.setOnClickListener {
        }
    }

    val listener = object : CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
            when(p0?.id){
                R.id.cbAll ->{
                    if(p1==true){
                        binding.cbAgree1.isChecked = true
                        binding.cbAgree2.isChecked = true
                    }else {
                        binding.cbAgree1.isChecked = false
                        binding.cbAgree2.isChecked = false
                    }
                }
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}