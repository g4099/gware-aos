package com.example.orca_gware.network

import android.content.Context
import android.os.Bundle
import android.util.Log
import com.example.orca_gware.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

object ApiService {
    private val TAG = "ApiService"
    private var mRetrofit: Retrofit? = null
    private var mRetrofitForSearch: Retrofit? = null
    private val REQUEST_TIMEOUT = 60
    private var connectionTime = 1
    private val response: String? = null

    @JvmStatic
    var httpResponse: Response? = null

    fun provideRetrofit(context: Context): Retrofit? {
        val gson = GsonBuilder()
            .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
            .setPrettyPrinting()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
        mRetrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASEURL)
            .client(provideOkHttpClient(context))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(NullOnEmptyConverterFactory())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return mRetrofit
    }

    fun provideRetrofitForImage(context: Context): Retrofit? {
        val gson = GsonBuilder()
            .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
            .setPrettyPrinting()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
        mRetrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASEIMAGEURL)
            .client(provideOkHttpClient(context))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(NullOnEmptyConverterFactory())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return mRetrofit
    }

    private fun provideOkHttpClient(context: Context): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(provideApiKeyInterceptor(context)!!)
            //TODO 로그 확인 시 필요
//            .addInterceptor(provideHttpLoggingInterceptor()!!)
            .addInterceptor(provideAuthInterceptor(context)!!)
//            .addInterceptor(Interceptor { chain ->
//                var request = chain.request()
//                var response = chain.proceed(request)
//                Log.d(TAG, "provideOkHttpClient: response : ${response.headers.toString().contains("ETag")}")
//                if(response.headers.toString().contains("ETag")){
//                    var data = Bundle()
//                    data.putString("from","ApiService")
//                    data.putString("event", "putComplete")
//                    RxEventBus.getInstance()!!.post(data)
//                }
//                response
//            })
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()
    }

    private fun provideApiKeyInterceptor(context: Context?): Interceptor? {
        return Interceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            val originalHttpUrl = original.url
            val urlBuilder = originalHttpUrl.newBuilder()
            val url = urlBuilder.build()
            //401
            val requestBuilder = original.newBuilder()
                .url(url)
            if (context != null) {
            }
            val request = requestBuilder.build()
            chain.proceed(request)
        }
    }


    private fun provideAuthInterceptor(context: Context?): Interceptor? {
        return Interceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            val originalHttpUrl = original.url
            val urlBuilder = originalHttpUrl.newBuilder()
            val url = urlBuilder.build()
            val requestBuilder = original.newBuilder()
                .url(url)
            if (context != null) {
            }

            // try the request
            val response = chain.proceed(original)
            createWellDoneResponse(response, chain, requestBuilder, context)!!
        }
    }

    private fun createWellDoneResponse(
        response: Response,
        chain: Interceptor.Chain,
        requestBuilder: Request.Builder,
        context: Context?
    ): Response? {
        httpResponse = response
        if (response.code == 401) {
            connectionTime++
            return response
        }
        return response
    }

    private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor? {
        val interceptor = HttpLoggingInterceptor()
        val logLevel =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        var logHeader = HttpLoggingInterceptor.Level.HEADERS
        Log.d(TAG, "provideHttpLoggingInterceptor: $logHeader")
        interceptor.setLevel(logLevel)
        return interceptor
    }

    fun <T> provideApi(service: Class<T>?, context: Context): T {
        return provideRetrofit(context)!!.create(service)
    }


    fun <T> provideImageApi(service: Class<T>?, context: Context): T {
//        return provideRetrofitForKakao(context)!!.create(service)
        return provideRetrofitForImage(context)!!.create(service)
    }

    class NullOnEmptyConverterFactory : Converter.Factory() {
        override fun responseBodyConverter(
            type: Type?,
            annotations: Array<Annotation>?,
            retrofit: Retrofit?
        ): Converter<ResponseBody, *>? {
            val delegate = retrofit!!.nextResponseBodyConverter<Any>(this, type!!, annotations!!)
            return Converter<ResponseBody, Any> {
                if (it.contentLength() == 0L) return@Converter null
                delegate.convert(it)
            }
        }

    }

}