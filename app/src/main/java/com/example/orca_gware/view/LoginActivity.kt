package com.example.orca_gware.view

import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.orca_gware.R
import com.example.orca_gware.databinding.ActivityLoginBinding
import com.example.orca_gware.fragment.*

class LoginActivity : AppCompatActivity() {

    private lateinit var binding : ActivityLoginBinding
    val loginFrag = LoginFragment()
    val termsFrag = TermsFragment()
    val signUpFrag = SignUpFragment()
    val phoneNumFrag = PhoneNumCheckFragment()
    val businessNumFrag = BusinessNumFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setFrag("Login")
    }

    fun setFrag(name:String){
        when(name){
            "Login" -> {
                val manager = supportFragmentManager
                val tran = manager.beginTransaction()
                tran.replace(R.id.container, loginFrag )
                tran.commit()
            }
            "Terms" -> {
                val manager = supportFragmentManager
                val tran = manager.beginTransaction()
                tran.replace(R.id.container, termsFrag)
                tran.addToBackStack(null)
                tran.commit()
            }
            "SignUp" -> {
                val manager = supportFragmentManager
                val tran = manager.beginTransaction()
                tran.replace(R.id.container, signUpFrag)
                tran.addToBackStack(null)
                tran.commit()
            }
            "PhoneNum" -> {
                val manager = supportFragmentManager
                val tran = manager.beginTransaction()
                tran.replace(R.id.container, phoneNumFrag)
                tran.addToBackStack(null)
                tran.commit()
            }
            "BusinessNum" -> {
                val manager = supportFragmentManager
                val tran = manager.beginTransaction()
                tran.replace(R.id.container, businessNumFrag)
                tran.addToBackStack(null)
                tran.commit()
            }
        }
    }
}